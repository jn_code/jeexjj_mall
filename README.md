#### 特别致谢
- 架构借鉴[xmall](https://gitee.com/Exrick/xmall)项目，本前端项目[mall-front](https://gitee.com/zhanghejie/jeexjj_mall/mall-front)改自[xmall-front](https://gitee.com/Exrick/xmall-front)
- 向xmall致敬https://gitee.com/Exrick/xmall.
- 感谢 [yucccc](https://github.com/yucccc) 的开源[vue-mall](https://github.com/yucccc/vue-mall) 项目提供前端页面及框架支持

#### 项目介绍
- 是基于jeexjj轻量级快速开发框架开发的商城。QQ交流群：174266358。
- 基于springboot的微服务模式，可平滑升级为微服务高并发架构。
- 高仿锤子科技商城，商城前端使用vue2.0前后端分离，后端使用jeexjj框架开发。
- 了解jeexjj轻量级快速开发框架，请移步 [码云开源地址](https://gitee.com/zhanghejie/jeexjj) 或  [github开源地址](https://github.com/zhanghejie/jeexjj)。
- jeexjj_mall是基于jeexjj轻量级快速开发框架开发的高仿锤子科技商城，商城前端使用vue2.0前后端分离，自适应移动端，帮助中小企业快速搭自已的B2C商城。
- 使用redis缓存，提高商城的响应速度，减轻数据的压力。


#### 项目架构
- jeexjj-mall-admin 是商城的后端,基于springboot,是一个maven项目。
- jeexjj-mall-front 是商城的前端,基于vue2.0,是一个前端项目.

#### 技术选型
#### 后端技术:
技术 | 名称 | 官网
----|------|----
redis | nosql DB  | [https://redis.io/](https://redis.io/)
spring-boot | 后端框架  | [http://spring.io/projects/spring-boot](http://spring.io/projects/spring-boot)
Spring Framework | 容器  | [http://projects.spring.io/spring-framework/](http://projects.spring.io/spring-framework/)
SpringMVC | MVC框架  | [http://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/#mvc](http://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/#mvc)
MyBatis | ORM框架  | [http://www.mybatis.org/mybatis-3/zh/index.html](http://www.mybatis.org/mybatis-3/zh/index.html)
Maven | 项目构建管理  | [http://maven.apache.org/](http://maven.apache.org/)
freemarker | 页面模板引擎  | [http://freemarker.foofun.cn/](http://freemarker.foofun.cn/)


#### 前端技术:
技术 | 名称 | 官网
----|------|----
Vuejs | 前端框架  | [https://vuejs.org/](https://vuejs.org/)
nodejs | JavaScript engine  | [https://nodejs.org/](https://nodejs.org/)
Bootstrap | 前端框架  | [http://getbootstrap.com/](http://getbootstrap.com/)
Font-awesome | 字体图标  | [http://fontawesome.io/](http://fontawesome.io/)
zTree | 树插件  | [http://www.treejs.cn/v3/](http://www.treejs.cn/v3/)
layui | 前端框架  | [http://layer.layui.com/](http://layer.layui.com/)
ace | 前端框架  | [http://ace.jeka.by/](http://ace.jeka.by/)


#### 后台管理项目mall-admin安装教程
1. 安装jdk1.8+、mysql5.7+、maven3.5+、redis
2. 下载代码并初始化数据库doc/db/mall_*.sql
3. 下载myeclipse2017(后面有地址)，导入maven项目 jeexjj-mall-admin
4. 每个项目依次执行maven install
5. 运行jeexjj-mall-admin的application.java的main方法。
6. 访问http://localhost:8883

#### 商城mall-front运行步骤
1. 运行springboot项目jeexjj-mall-admin
2. 本地安装node3.js
3. 在jeexjj-mall-front根目录运行命令npm install 等待执行完毕
4. 在jeexjj-mall-front根目录运行命令npm run start即可在浏览器中 的localhost:9999查看项目

#### 技术交流
1. QQ群：174266358
2. 开发工具下载地址：https://pan.baidu.com/s/1BXnWGkASzmYDroIYtJbCBg

#### 后台管理端页面效果
![](doc/images/admin1.png)
![](doc/images/admin2.png)

#### 商城页面效果
![](doc/images/mall1.png)
![](doc/images/mall2.png)
![](doc/images/mall3.png)
![](doc/images/mall4.png)
