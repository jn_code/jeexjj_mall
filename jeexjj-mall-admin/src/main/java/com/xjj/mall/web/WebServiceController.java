package com.xjj.mall.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonObject;
import com.xjj.mall.pojo.Member;
import com.xjj.mall.service.LoginService;
import com.xjj.mall.webservice.MainService;

import io.swagger.annotations.ApiOperation;

@Controller
@RequestMapping("web")
public class WebServiceController {
	public static final String ALL = "ALL";
	public static final String USER_PASS_ERROR = "用户存在未激活或者密码错误";
	public static final String PASS_ERROR = "密码错误";
	public static final String USER_NULL = "用户不存在";
	@Autowired
    private LoginService loginService;
	//登陆信息
	@RequestMapping(value = "/getloginInfo",method = RequestMethod.GET)
    @ApiOperation(value = "获取用户登录")
	public  @ResponseBody Object getLogin(String users, String password){
		Member member=loginService.userLogin2(users, password);
		return member;
	}
	
	//登陆信息
	@RequestMapping(value = "/CreditsChange",method = RequestMethod.GET)
    @ApiOperation(value = "改变积分")
	public  @ResponseBody Object CreditsChange(String uid, String ys,String jifen,String str){
		String value="BET|"+uid+"|"+ys+"|"+jifen+"|"+str+"|CREDITS";
		String s=loginService.CreditsChange(value);
		return s;
	}

}
