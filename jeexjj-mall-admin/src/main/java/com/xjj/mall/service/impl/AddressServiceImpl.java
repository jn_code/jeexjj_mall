package com.xjj.mall.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xjj.framework.dao.XjjDAO;
import com.xjj.framework.service.XjjServiceSupport;
import com.xjj.framework.web.support.XJJParameter;
import com.xjj.mall.dao.AddressDao;
import com.xjj.mall.entity.AddressEntity;
import com.xjj.mall.service.AddressService;

/**
 * @author zhanghejie
 */
@Service
public class AddressServiceImpl extends XjjServiceSupport<AddressEntity> implements AddressService {

    @Autowired
    private AddressDao addressDao;
    
    @Override
	public XjjDAO<AddressEntity> getDao() {
		
		return addressDao;
	}

    @Override
    public List<AddressEntity> getAddressList(Long userId) {

//        List<AddressEntity> list=new ArrayList<>();
//        TbAddressExample example=new TbAddressExample();
//        TbAddressExample.Criteria criteria=example.createCriteria();
//        criteria.andUserIdEqualTo(userId);
//        list=tbAddressMapper.selectByExample(example);
//        if(list==null){
//            throw new XmallException("获取默认地址列表失败");
//        }
//
//        for(int i=0;i<list.size();i++){
//            if(list.get(i).getIsDefault()){
//                Collections.swap(list,0,i);
//                break;
//            }
//        }
    	XJJParameter param = new XJJParameter();
    	param.addQuery("query.userId@eq@l",userId);
    	param.addOrderByDesc("isDefault");
    	List<AddressEntity> list = addressDao.findList(param.getQueryMap());
        return list;
    }
}
