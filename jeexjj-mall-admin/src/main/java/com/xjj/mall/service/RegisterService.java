package com.xjj.mall.service;

import com.xjj.mall.entity.MemberEntity;

/**
 * @author zhanghejie
 */
public interface RegisterService {

    /**
     * 勾选
     * @param param
     * @param type
     * @return
     */
    boolean checkData(String param, int type);

    /**
     * 注册
     * @param userName
     * @param userPwd
     * @return
     */
    int register(String userName,String userPwd);
    /**
     * 注册2
     * @param userName
     * @param userPwd
     * @return
     */
    int register2(MemberEntity tbMember);
}
