package com.xjj.mall.service.impl;


import com.google.gson.Gson;
import com.xjj.framework.web.support.XJJParameter;
import com.xjj.mall.common.jedis.JedisClient;
import com.xjj.mall.dao.MemberDao;
import com.xjj.mall.entity.MemberEntity;
import com.xjj.mall.pojo.Member;
import com.xjj.mall.service.LoginService;
import com.xjj.mall.util.DtoUtil;
import com.xjj.mall.webservice.MainService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @author zhanghejie
 */
@Service
public class LoginServiceImpl implements LoginService {
	@Autowired
	private MemberDao memberDao;
	@Autowired
	private JedisClient jedisClient;
	@Value("${SESSION_EXPIRE}")
	private Integer SESSION_EXPIRE;
	@Override
	public String getCredits(String str) {
		str = "BET|"+str+"|CREDITS";
		MainService main = new MainService();
		String value =main.getMainServiceSoap().getCredits(str);
		return value;
	}
	@Override
	public String CreditsChange(String str) {
		MainService main = new MainService();
		String value =main.getMainServiceSoap().setCreditsChange(str);
		return value;
	}

	@Override
	public Member userLogin2(String username, String password) {
		String str = username+"|"+password;
		MainService main = new MainService();
		String str2 =main.getMainServiceSoap().getLogin(str);
		String [] value = str2.split("\\|");
		Member member = new Member();
		if(value.length>0 && !value[0].equals("-1")) {
			member.setUsername(value[4]);
			member.setPhone(value[4]);
			String jifen=value[41];
			member.setPoints(Integer.parseInt(jifen.substring(0,jifen.indexOf("."))));
			member.setDescription(value[1]);
		}
		return member;
	}
	@Override
	public Member userLogin(String username, String password, Integer points) {

//		TbMemberExample example = new TbMemberExample();
//		TbMemberExample.Criteria criteria = example.createCriteria();
//		criteria.andStateEqualTo(1);
//		criteria.andUsernameEqualTo(username);
//		List<TbMember> list = tbMemberMapper.selectByExample(example);
//		if (list == null || list.size() == 0) {
//			Member member=new Member();
//			member.setState(0);
//			member.setMessage("用户名或密码错误");
//			return member;
//		}
//		TbMember tbMember = list.get(0);
		
		XJJParameter param = new XJJParameter();
		param.addQuery("query.state@eq@i",1);
		param.addQuery("query.username@eq@s",username);
		List<MemberEntity> list = memberDao.findList(param.getQueryMap());
		
		if (list == null || list.size() == 0) {
			Member member=new Member();
			member.setState(0);
			member.setMessage("用户名或密码错误");
			return member;
		}
		MemberEntity tbMember = list.get(0);
		
		//md5加密
		if (!DigestUtils.md5DigestAsHex(password.getBytes()).equals(tbMember.getPassword())) {
			Member member=new Member();
			member.setState(0);
			member.setMessage("用户名或密码错误");
			return member;
		}
		String token = UUID.randomUUID().toString();
		Member member= DtoUtil.TbMemer2Member(tbMember);
		member.setToken(token);
		member.setState(1);
		member.setPoints(points);
		// 用户信息写入redis：key："SESSION:token" value："user"
		jedisClient.set("SESSION:" + token, new Gson().toJson(member));
		jedisClient.expire("SESSION:" + token, SESSION_EXPIRE);

		return member;
	}

	@Override
	public Member getUserByToken(String token) {

		String json = jedisClient.get("SESSION:" + token);
		if (json==null) {
			Member member=new Member();
			member.setState(0);
			member.setMessage("用户登录已过期");
			return member;
		}
		//重置过期时间
		//jedisClient.expire("SESSION:" + token, SESSION_EXPIRE);
		Member member = new Gson().fromJson(json,Member.class);
		return member;
	}

	@Override
	public int logout(String token) {
		jedisClient.del("SESSION:" + token);
		return 1;
	}

	

	

}
