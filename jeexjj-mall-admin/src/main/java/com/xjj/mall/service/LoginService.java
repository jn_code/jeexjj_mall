package com.xjj.mall.service;

import com.xjj.mall.pojo.Member;

/**
 * @author zhanghejie
 */
public interface LoginService {
	/**
     * 获取用户积分
     * @param str
     * @return
     */
	String getCredits(String str);
	/**
     * 改变积分
     * @param username
     * @param password
     * @return
     */
	String CreditsChange(String str);
	/**
     * 获取易飞用户信息2
     * @param username
     * @param password
     * @return
     */
    Member userLogin2(String username, String password);
    /**
     * 登录
     * @param username
     * @param password
     * @return
     */
    Member userLogin(String username, String password, Integer points);

    /**
     * 通过token获取
     * @param token
     * @return
     */
    Member getUserByToken(String token);

    /**
     * 注销
     * @param token
     * @return
     */
    int logout(String token);
}
