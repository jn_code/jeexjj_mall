package com.xjj.mall.service.impl;

import com.google.gson.Gson;
import com.xjj.framework.dao.XjjDAO;
import com.xjj.framework.service.XjjServiceSupport;
import com.xjj.mall.common.exception.XmallException;
import com.xjj.mall.common.jedis.JedisClient;
import com.xjj.mall.common.utils.QiniuUtil;
import com.xjj.mall.dao.MemberDao;
import com.xjj.mall.entity.MemberEntity;
import com.xjj.mall.entity.OrderItemEntity;
import com.xjj.mall.pojo.Member;
import com.xjj.mall.service.LoginService;
import com.xjj.mall.service.MemberService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * @author zhanghejie
 */
@Service
public class MemberServiceImpl  extends XjjServiceSupport<MemberEntity> implements MemberService {

    @Autowired
    private LoginService loginService;
    @Autowired
    private MemberDao memberDao;
    @Autowired
    private JedisClient jedisClient;
    @Value("${SESSION_EXPIRE}")
    private Integer SESSION_EXPIRE;

    @Override
	public XjjDAO<MemberEntity> getDao() {
		return memberDao;
	}
    
    @Override
    public String imageUpload(Long userId,String token,String imgData) {

        //过滤data:URL
        String base64=QiniuUtil.base64Data(imgData);
        String imgPath= QiniuUtil.qiniuBase64Upload(base64);

        MemberEntity tbMember=memberDao.getById(userId);
        if(tbMember==null){
            throw new XmallException("通过id获取用户失败");
        }
        tbMember.setFile(imgPath);
        memberDao.update(tbMember);

        //更新缓存
        Member member=loginService.getUserByToken(token);
        member.setFile(imgPath);
        jedisClient.set("SESSION:" + token, new Gson().toJson(member));
        return imgPath;
    }
	
}
