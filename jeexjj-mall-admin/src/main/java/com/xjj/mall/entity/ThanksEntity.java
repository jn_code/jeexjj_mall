/****************************************************
 * Description: Entity for t_mall_thanks
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.entity;

import java.util.Date;
import java.math.BigDecimal;
import com.xjj.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ThanksEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public ThanksEntity(){}
    private String nickName;//nick_name
    private String username;//username
    private BigDecimal money;//money
    private String info;//info
    private String email;//通知邮箱
    private Integer state;//状态 0待审核 1确认显示  2驳回 3通过不展示
    private String payType;//支付方式
    private String orderId;//关联订单id
    private Date createDate;//date
    private Long userId;
    /**
     * 返回nick_name
     * @return nick_name
     */
    public String getNickName() {
        return nickName;
    }
    
    /**
     * 设置nick_name
     * @param nickName nick_name
     */
    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    
    /**
     * 返回username
     * @return username
     */
    public String getUsername() {
        return username;
    }
    
    /**
     * 设置username
     * @param username username
     */
    public void setUsername(String username) {
        this.username = username;
    }
    
    /**
     * 返回money
     * @return money
     */
    public BigDecimal getMoney() {
        return money;
    }
    
    /**
     * 设置money
     * @param money money
     */
    public void setMoney(BigDecimal money) {
        this.money = money;
    }
    
    /**
     * 返回info
     * @return info
     */
    public String getInfo() {
        return info;
    }
    
    /**
     * 设置info
     * @param info info
     */
    public void setInfo(String info) {
        this.info = info;
    }
    
    /**
     * 返回通知邮箱
     * @return 通知邮箱
     */
    public String getEmail() {
        return email;
    }
    
    /**
     * 设置通知邮箱
     * @param email 通知邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }
    
    /**
     * 返回状态 0待审核 1确认显示  2驳回 3通过不展示
     * @return 状态 0待审核 1确认显示  2驳回 3通过不展示
     */
    public Integer getState() {
        return state;
    }
    
    /**
     * 设置状态 0待审核 1确认显示  2驳回 3通过不展示
     * @param state 状态 0待审核 1确认显示  2驳回 3通过不展示
     */
    public void setState(Integer state) {
        this.state = state;
    }
    
    /**
     * 返回支付方式
     * @return 支付方式
     */
    public String getPayType() {
        return payType;
    }
    
    /**
     * 设置支付方式
     * @param payType 支付方式
     */
    public void setPayType(String payType) {
        this.payType = payType;
    }
    
    /**
     * 返回关联订单id
     * @return 关联订单id
     */
    public String getOrderId() {
        return orderId;
    }
    
    /**
     * 设置关联订单id
     * @param orderId 关联订单id
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("com.xjj.mall.entity.ThanksEntity").append("ID="+this.getId()).toString();
    }
}

