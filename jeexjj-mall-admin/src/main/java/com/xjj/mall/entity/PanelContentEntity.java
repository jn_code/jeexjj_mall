/****************************************************
 * Description: Entity for t_mall_panel_content
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.entity;

import java.math.BigDecimal;
import java.util.Date;
import com.xjj.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class PanelContentEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public PanelContentEntity(){}
    private Long panelId;//所属板块id
    private Integer type;//类型 0关联商品 1其他链接
    private Long productId;//关联商品id
    private Integer sortOrder;//sort_order
    private String fullUrl;//其他链接
    private String picUrl;//pic_url
    private String picUrl2;//3d轮播图备用
    private String picUrl3;//3d轮播图备用
    private Date created;//created
    private Date updated;//updated
    
    
    /**
     * 关联商品信息
     */
    private BigDecimal salePrice;

    private String productName;

    private String subTitle;

    private String productImageBig;
    /**
     * 返回所属板块id
     * @return 所属板块id
     */
    public Long getPanelId() {
        return panelId;
    }
    
    /**
     * 设置所属板块id
     * @param panelId 所属板块id
     */
    public void setPanelId(Long panelId) {
        this.panelId = panelId;
    }
    
    /**
     * 返回类型 0关联商品 1其他链接
     * @return 类型 0关联商品 1其他链接
     */
    public Integer getType() {
        return type;
    }
    
    /**
     * 设置类型 0关联商品 1其他链接
     * @param type 类型 0关联商品 1其他链接
     */
    public void setType(Integer type) {
        this.type = type;
    }
    
    /**
     * 返回关联商品id
     * @return 关联商品id
     */
    public Long getProductId() {
        return productId;
    }
    
    /**
     * 设置关联商品id
     * @param productId 关联商品id
     */
    public void setProductId(Long productId) {
        this.productId = productId;
    }
    
    /**
     * 返回sort_order
     * @return sort_order
     */
    public Integer getSortOrder() {
        return sortOrder;
    }
    
    /**
     * 设置sort_order
     * @param sortOrder sort_order
     */
    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }
    
    /**
     * 返回其他链接
     * @return 其他链接
     */
    public String getFullUrl() {
        return fullUrl;
    }
    
    /**
     * 设置其他链接
     * @param fullUrl 其他链接
     */
    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }
    
    /**
     * 返回pic_url
     * @return pic_url
     */
    public String getPicUrl() {
        return picUrl;
    }
    
    /**
     * 设置pic_url
     * @param picUrl pic_url
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }
    
    /**
     * 返回3d轮播图备用
     * @return 3d轮播图备用
     */
    public String getPicUrl2() {
        return picUrl2;
    }
    
    /**
     * 设置3d轮播图备用
     * @param picUrl2 3d轮播图备用
     */
    public void setPicUrl2(String picUrl2) {
        this.picUrl2 = picUrl2;
    }
    
    /**
     * 返回3d轮播图备用
     * @return 3d轮播图备用
     */
    public String getPicUrl3() {
        return picUrl3;
    }
    
    /**
     * 设置3d轮播图备用
     * @param picUrl3 3d轮播图备用
     */
    public void setPicUrl3(String picUrl3) {
        this.picUrl3 = picUrl3;
    }
    
    /**
     * 返回created
     * @return created
     */
    public Date getCreated() {
        return created;
    }
    
    /**
     * 设置created
     * @param created created
     */
    public void setCreated(Date created) {
        this.created = created;
    }
    
    /**
     * 返回updated
     * @return updated
     */
    public Date getUpdated() {
        return updated;
    }
    
    /**
     * 设置updated
     * @param updated updated
     */
    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public BigDecimal getSalePrice() {
		return salePrice;
	}

	public void setSalePrice(BigDecimal salePrice) {
		this.salePrice = salePrice;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getProductImageBig() {
		return productImageBig;
	}

	public void setProductImageBig(String productImageBig) {
		this.productImageBig = productImageBig;
	}

	public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("com.xjj.mall.entity.PanelContentEntity").append("ID="+this.getId()).toString();
    }
}

